FROM openjdk:11-jdk
WORKDIR /app
COPY . .
RUN chmod +x ./mvnw
RUN ./mvnw install -DskipTests
CMD ["java","-jar","target/data-handler.jar"]