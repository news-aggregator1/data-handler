package com.news.datahandler.app.handler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.networknt.schema.JsonSchema;
import com.networknt.schema.JsonSchemaFactory;
import com.networknt.schema.SpecVersion;
import com.networknt.schema.ValidationMessage;
import com.news.datahandler.app.data.dto.ArticleDto;
import com.news.datahandler.app.data.repository.ArticleRepository;
import com.news.datahandler.app.data.service.ArticleConverter;
import com.news.datahandler.app.data.service.ArticleService;
import org.json.JSONArray;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class ArticleMessageListener implements MessageListener {

    private Logger logger = LoggerFactory.getLogger(ArticleMessageListener.class);

    private ArticleRepository articleRepository;
    private ArticleService articleService;
    private ArticleConverter articleConverter;

    ArticleMessageListener(ArticleRepository articleRepository, ArticleService articleService, ArticleConverter articleConverter) {
        this.articleRepository = articleRepository;
        this.articleService = articleService;
        this.articleConverter = articleConverter;
    }

    @Override
    public void onMessage(Message message) {
        logger.info("Received message from " + message.getMessageProperties().getConsumerQueue() +
                " on exchange " + message.getMessageProperties().getReceivedExchange() +
                " with key " + message.getMessageProperties().getReceivedRoutingKey());
        logger.debug(message.toString());

        String messageContent = new String(message.getBody());
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            JSONArray json = new JSONArray(messageContent);

            Set<ValidationMessage> errors = validateWithJsonSchema(objectMapper, messageContent);
            List<ArticleDto> articles = objectMapper.readValue(json.toString(), new TypeReference<>() {});
            SchemaValidationHelper.removeInvalidArticles(articles, errors);

            logger.debug(articles.toString());
            articleRepository.saveAll(
                    articles.stream()
                            .filter(articleDto -> !articleService.alreadyExists(articleDto))
                            .map(articleDto -> articleConverter.convertArticleDtoToArticle(articleDto))
                            .collect(Collectors.toList())
            );
        } catch (JSONException | JsonProcessingException e) {
            logger.error(e.getMessage());
        }
    }

    private Set<ValidationMessage> validateWithJsonSchema(ObjectMapper objectMapper, String messageContent) {
        Set<ValidationMessage> errors = new HashSet<>();

        try (InputStream inputStream = getClass().getResourceAsStream("/articles.schema.json")) {
            JsonSchemaFactory schemaFactory = JsonSchemaFactory.getInstance(SpecVersion.VersionFlag.V201909);
            JsonSchema schema = schemaFactory.getSchema(inputStream);
            JsonNode messageNode = objectMapper.readTree(messageContent);

            errors = schema.validate(messageNode);

            if (!errors.isEmpty()) {
                logger.info(messageContent);
                logger.info(SchemaValidationHelper.getIndicesOfInvalidArticles(errors).size()
                        + " articles did not pass schema validation.");
            }

            for (ValidationMessage error : errors) {
                logger.info(error.getMessage());
            }

        } catch (IOException e) {
            logger.error(e.getMessage());
        }

        return errors;
    }

}
