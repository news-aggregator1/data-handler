package com.news.datahandler.app.handler;

import com.networknt.schema.ValidationMessage;
import com.news.datahandler.app.data.dto.ArticleDto;
import org.apache.commons.lang3.StringUtils;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class SchemaValidationHelper {

    public static List<ArticleDto> removeInvalidArticles(List<ArticleDto> articles, Set<ValidationMessage> errors) {
        List<Integer> indices = getIndicesOfInvalidArticles(errors);
        indices.sort(Comparator.reverseOrder());

        for (Integer index : indices) {
            articles.remove((int) index);
        }

        return articles;
    }

    public static List<Integer> getIndicesOfInvalidArticles(Set<ValidationMessage> errors) {
        return errors.stream()
                .map(e -> getIndexOfInvalidArticle(e.getPath()))
                .collect(Collectors.toList());
    }

    public static int getIndexOfInvalidArticle(String path) {
        return Integer.parseInt(StringUtils.substringBetween(path, "[", "]"));
    }
}
