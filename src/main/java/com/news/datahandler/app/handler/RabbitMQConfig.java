package com.news.datahandler.app.handler;

import com.news.datahandler.app.data.repository.ArticleRepository;
import com.news.datahandler.app.data.service.ArticleConverter;
import com.news.datahandler.app.data.service.ArticleService;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

    @Value("${rabbit.queue.name}")
    private String queueName;

    @Value("${rabbit.exchange.name}")
    private String exchangeName;

    @Value("${spring.rabbitmq.host}")
    private String rabbitHost;

    @Value("${spring.rabbitmq.port}")
    private String rabbitPort;

    @Value("${spring.rabbitmq.username}")
    private String rabbitUsername;

    @Value("${spring.rabbitmq.password}")
    private String rabbitPassword;

    @Bean
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
        connectionFactory.setAddresses(rabbitHost + ":" + rabbitPort);
        connectionFactory.setUsername(rabbitUsername);
        connectionFactory.setPassword(rabbitPassword);
        return connectionFactory;
    }

    @Bean
    public AmqpAdmin amqpAdmin() {
        AmqpAdmin amqpAdmin = new RabbitAdmin(connectionFactory());

        for (int i = 0;i < 4;i++) {
            String queueAndKeyName = queueName + "0" + (i+1);
            Queue queue = new Queue(queueAndKeyName, true, false, false);
            amqpAdmin.declareQueue(queue);
            try {
                amqpAdmin.declareBinding(BindingBuilder.bind(queue).to(exchange()).with(queueAndKeyName));
            } catch (Exception e) {
                amqpAdmin.declareExchange(exchange());
                amqpAdmin.declareBinding(BindingBuilder.bind(queue).to(exchange()).with(queueAndKeyName));
            }
        }

        return amqpAdmin;
    }

    @Bean
    public SimpleMessageListenerContainer listenerContainer(ConnectionFactory connectionFactory, ArticleMessageListener articleMessageListener) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);

        String[] queueNames = new String[4];

        for (int i = 0;i < 4;i++) {
            queueNames[i] = queueName + "0" + (i+1);
        }

        container.setQueueNames(queueNames);
        container.setMessageListener(articleMessageListener);

        return container;
    }

    @Bean
    public ArticleMessageListener articleMessageListener(ArticleRepository articleRepository, ArticleService articleService, ArticleConverter articleConverter) {
        return new ArticleMessageListener(articleRepository, articleService, articleConverter);
    }

    @Bean
    DirectExchange exchange() {
        return new DirectExchange(exchangeName, false, false);
    }
}
