package com.news.datahandler.app.data.service;

import com.news.datahandler.app.data.dto.ArticleDto;
import com.news.datahandler.app.data.entity.Article;
import com.news.datahandler.app.data.entity.Author;
import com.news.datahandler.app.data.entity.Category;
import com.news.datahandler.app.data.entity.Source;
import com.news.datahandler.app.data.entity.Tag;
import com.news.datahandler.app.data.repository.AuthorRepository;
import com.news.datahandler.app.data.repository.CategoryRepository;
import com.news.datahandler.app.data.repository.SourceRepository;
import com.news.datahandler.app.data.repository.TagRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ArticleConverter {

    private AuthorRepository authorRepository;
    private CategoryRepository categoryRepository;
    private SourceRepository sourceRepository;
    private TagRepository tagRepository;

    public ArticleConverter(AuthorRepository authorRepository, CategoryRepository categoryRepository,
                            SourceRepository sourceRepository, TagRepository tagRepository) {
        this.authorRepository = authorRepository;
        this.categoryRepository = categoryRepository;
        this.sourceRepository = sourceRepository;
        this.tagRepository = tagRepository;
    }

    public Article convertArticleDtoToArticle(ArticleDto articleDto) {
        Article article = new Article();
        article.setTitle(articleDto.getTitle());
        article.setDescription(articleDto.getDescription());
        article.setUrl(articleDto.getUrl());
        article.setImageUrl(articleDto.getImageUrl());
        article.setPublicationDate(articleDto.getPublicationDate());
        article.setContent(articleDto.getContent());

        article.setSource(getSource(articleDto.getSource()));
        article.setCategory(getCategory(articleDto.getCategory()));
        article.setAuthors(getAuthors(articleDto.getAuthors()));
        article.setTags(getTags(articleDto.getTags()));

        return article;
    }

    private Source getSource(String name) {
        Optional<Source> source = sourceRepository.findByName(name);

        if (source.isPresent()) {
            return source.get();
        } else {
            Source source1 = new Source(name);
            sourceRepository.save(source1);
            return source1;
        }
    }

    private Category getCategory(String name) {
        Optional<Category> category = categoryRepository.findByName(name);

        if (category.isPresent()) {
            return category.get();
        } else {
            Category category1 = new Category(name);
            categoryRepository.save(category1);
            return category1;
        }
    }

    private List<Author> getAuthors(List<String> names) {
        List<Author> authors = new ArrayList<>();

        for (String name: names) {
            Optional<Author> author = authorRepository.findByName(name);

            if (author.isPresent()) {
                authors.add(author.get());
            } else {
                Author author1 = new Author(name);
                authors.add(author1);
                authorRepository.save(author1);
            }
        }

        return authors;
    }

    private List<Tag> getTags(List<String> names) {
        List<Tag> tags = new ArrayList<>();

        for (String name: names) {
            Optional<Tag> tag = tagRepository.findByName(name);

            if (tag.isPresent()) {
                tags.add(tag.get());
            } else {
                Tag tag1 = new Tag(name);
                tags.add(tag1);
                tagRepository.save(tag1);
            }
        }

        return tags;
    }


}
