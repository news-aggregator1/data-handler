package com.news.datahandler.app.data.service;

import com.news.datahandler.app.data.dto.ArticleDto;
import com.news.datahandler.app.data.repository.ArticleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class ArticleService {

    private Logger logger = LoggerFactory.getLogger(ArticleService.class);

    private ArticleRepository articleRepository;

    public ArticleService(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    public boolean alreadyExists(ArticleDto articleDto) {
        boolean alreadyExists = articleRepository.existsWithSourceAndTitle(articleDto.getSource(), articleDto.getTitle()) > 0;

        if (alreadyExists) {
            logger.debug("Rejected article: " + articleDto);
        }

        return alreadyExists;
    }

}
