package com.news.datahandler.app.data.repository;

import com.news.datahandler.app.data.entity.Article;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticleRepository extends JpaRepository<Article, Long> {

        @Query("SELECT COUNT(art.id) " +
            "FROM Article art LEFT JOIN Source src ON art.source.id = src.id " +
            "WHERE src.name = :source AND art.title = :title ")
    int existsWithSourceAndTitle(String source, String title);

}
