package com.news.datahandler.app.data.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ArticleDto implements Serializable {
    private String source;
    private String title;
    private String description;
    private String url;
    private String imageUrl;
    private int publicationDate;
    private String content;
    private List<String> authors;
    private List<String> tags;
    private String category;
}
